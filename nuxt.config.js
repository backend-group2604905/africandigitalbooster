export default defineNuxtConfig({
  css:['~/assets/css/main.css'],
  vue: {
    plugins: [
      'Particles'
    ]
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
})